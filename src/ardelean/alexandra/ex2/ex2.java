package ardelean.alexandra.ex2;

        import javax.swing.*;
        import java.io.FileWriter;
        import java.io.IOException;

class ex2 extends JFrame {
    JLabel textLabel1;
    JLabel textLabel2;
    JTextField textField1;
    JTextField textField2;
    JButton jButton;
    ex2() {
        setSize(500, 200); //set frames size in pixels
        this.setLayout(null);
        textLabel1 = new JLabel("Nume fisier:");
        textLabel1.setBounds(10,20,120,20);

        textField1 = new JTextField("");
        textField1.setBounds(140,20,250,20);

        textLabel2 = new JLabel("Introduceti textul:: ");
        textLabel2.setBounds(10,60,120,20);

        textField2 = new JTextField("");
        textField2.setBounds(140,60,250,20);

        jButton = new JButton("Copiati textul in fisier:");
        jButton.setBounds(180,100,130,40);
        jButton.addActionListener(e -> {
            try (
                    FileWriter out = new FileWriter(textField1.getText())) {
                out.write(textField2.getText());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        add(textLabel1);
        add(textField1);
        add(textLabel2);
        add(textField2);
        add(jButton);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        ex2 gui = new ex2();
    }
}